<section class="header">
    <div class="header-brand">
        <a href="/">
            <img src="{{ asset('/static/vikinger.svg') }}" alt="Logo">
        </a>
    </div>
</section>
